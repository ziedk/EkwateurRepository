# EkwateurRepository

Bonjour,
Ci-joint le code d'une application backend développé avec spring boot .

Pour tester ce qui est demandé,vous devez 
1 - lancer l'application
2- faire un appel  web service "post" en utilisant l'URL 
http://localhost:8080/ekwateur/facture

et en indiquant dans le corp de la requete en tant que json :
{
  "reference": "EKW45555283",
  "consoElectricite": 255.25,
  "consoGaz": 3.33 ,
  "dateDebut" : "2023-02-01" ,
  "chiffreAffaire": 10000000
}

Explication : ce qui diffencie un client pro d'un client particulier,c'est le champ "chiffreAffaire".
Donc Si vous voulez avoir le montant de la facture pour un particulier,vous pouvez simplement supprimer le champ "chiffreAffaire" du corps de la requete post.

