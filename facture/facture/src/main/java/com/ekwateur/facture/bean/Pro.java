package com.ekwateur.facture.bean;

public class Pro extends Client{
	String nSiret ;
	String raisonSociale ;
	Double chiffreAffaire ;
	
	public String getnSiret() {
		return nSiret;
	}
	public void setnSiret(String nSiret) {
		this.nSiret = nSiret;
	}
	public String getRaisonSociale() {
		return raisonSociale;
	}
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}
	public Double getChiffreAffaire() {
		return chiffreAffaire;
	}
	public void setChiffreAffaire(Double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}
	
	
	
}
