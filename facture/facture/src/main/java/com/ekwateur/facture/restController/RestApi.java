package com.ekwateur.facture.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ekwateur.facture.services.FacturationService;


@RestController
@RequestMapping(value="/ekwateur")
@CrossOrigin(origins = "*")
public class RestApi {

	@Autowired
	private FacturationService facturationService;
	
	
	@PostMapping(path="/facture") 
	 public @ResponseBody String getMontantFacture (@RequestBody String json) {
		
		return facturationService.convertirVerifierCalculer(json) ;
		
			
			
		
		
		

	  }
	
}
