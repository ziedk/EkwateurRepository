package com.ekwateur.facture.services;



import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.ekwateur.facture.bean.Pro;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class FacturationService {
	
	
	private final double prixElecParticilier =  0.121 ;
	private final double prixGazParticilier = 0.115 ;
	
	private final double prixElecProSup = 0.114 ;
	private final double prixGazProSup =  0.111 ;
	
	private final double prixElecProInf = 0.118 ;
	private final double prixGazProInf =  0.113 ;
	
	private final int palierChiffreAffaire = 1000000 ;
	
	public String convertirVerifierCalculer(String json)
	{
		//on cast le client en tant que pro pour pouvoir tester le champ chiffre affaire
		ObjectMapper objectMapper = new ObjectMapper();
		
		Pro client;
		try {
			client = objectMapper.readValue(json, Pro.class);
			
			return calculMontant(client);
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Veuillez vérifier la structure des données transmise";
		}	
	}
	
	private String calculMontant(Pro client)
	{
		//verifier la reference du client
		String verifierReferenceClient = verifierReferenceClient(client.getReference());
		if(verifierReferenceClient != null)
		{
			return verifierReferenceClient;
		}
		
		double montantElectricite = client.getConsoElectricite();
		double montantGaz = client.getConsoGaz();
		
		if( client.getChiffreAffaire() != null)
		{
			if(client.getChiffreAffaire() > palierChiffreAffaire)
			{
				montantElectricite *= prixElecProSup ;
				montantGaz *= prixGazProSup ;
			}
			else
			{
				montantElectricite *= prixElecProInf ;
				montantGaz *= prixGazProInf ;
			}
		}
		else 
		{	//client particulier,pas besoin de caster car toute les données dont on a besoin sont dans la classe client
			montantElectricite *= prixElecParticilier ;
			montantGaz *= prixGazParticilier ;
		}
		
		/*Si on veut plus du détails du client,on pourra utiliser précisément soit
		 *  un client pro ou un client particulier.
		 *  Pour cet exercice , j'ai utiliser uniquement les informations de base  
		*/ 
		return "Pour la période de "
				.concat(client.getDateDebut().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)))
				.concat(" à ")
				.concat(client.getDateDebut().plusMonths(1).minusDays(1).format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)))
				.concat(" , Le montant de la consomation est ")
				.concat(String.valueOf(montantElectricite+montantGaz))
				;
		
	}
	
	private String verifierReferenceClient (String reference)
	{
		if(reference == null)
			return "Veillez renseigner la réference du client";
		
		if( !reference.startsWith("EKW")
			||
			!StringUtils.isNumeric(reference.substring(3))
			||
			!(reference.substring(3).length() == 8)
		   )
			return "La réference du client n'est pas valide";
		
		return null ;
	}
	
	
}


