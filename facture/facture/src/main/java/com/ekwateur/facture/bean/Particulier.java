package com.ekwateur.facture.bean;

public class Particulier extends Client {

	String civilité;
	String nom;
	String prénom;
	
	public String getCivilité() {
		return civilité;
	}
	public void setCivilité(String civilité) {
		this.civilité = civilité;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrénom() {
		return prénom;
	}
	public void setPrénom(String prénom) {
		this.prénom = prénom;
	}
	
	
	
}
