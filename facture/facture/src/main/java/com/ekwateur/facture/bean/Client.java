package com.ekwateur.facture.bean;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

public class Client {

	String reference ;
	float consoElectricite ;
	float consoGaz ;
	
	@JsonDeserialize(using = LocalDateDeserializer.class)
	LocalDate dateDebut ;
	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public float getConsoElectricite() {
		return consoElectricite;
	}

	public void setConsoElectricite(float consoElectricite) {
		this.consoElectricite = consoElectricite;
	}

	public float getConsoGaz() {
		return consoGaz;
	}

	public void setConsoGaz(float consoGaz) {
		this.consoGaz = consoGaz;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	
	
}
